package com.lawencon.user.dto;

public class ErrorRes {

	private String message;

	public ErrorRes(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
