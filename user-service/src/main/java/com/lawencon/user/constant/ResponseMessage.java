package com.lawencon.user.constant;

public enum ResponseMessage {
	INVALID_TOKEN("Invalid Token"), WRONG_USERNAME_PASWORD("Wrong username or password");

	private String message;

	private ResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
