package com.lawencon.user.exception;

public class InvalidLoginException extends RuntimeException {

	private static final long serialVersionUID = 8927873371454826370L;

	public InvalidLoginException() {
		super();
	}

	public InvalidLoginException(String message) {
		super(message);
	}

	public InvalidLoginException(Throwable t) {
		super(t);
	}

	public InvalidLoginException(String message, Throwable t) {
		super(message, t);
	}
}
