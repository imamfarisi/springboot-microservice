package com.lawencon.user.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.lawencon.user.dto.user.UserGetByIdResDto;
import com.lawencon.user.dto.user.UserListResDto;
import com.lawencon.user.dto.user.UserSaveReqDto;
import com.lawencon.user.dto.user.UserSaveResDto;
import com.lawencon.user.exception.InvalidLoginException;
import com.lawencon.user.exception.ValidateException;
import com.lawencon.user.model.User;
import com.lawencon.user.repo.UserRepo;
import com.lawencon.user.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	private final UserRepo userRepo;
	private final PasswordEncoder passwordEncoder;
	
	public UserServiceImpl(UserRepo userRepo, PasswordEncoder passwordEncoder) {
		this.userRepo = userRepo;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public UserSaveResDto save(UserSaveReqDto data) throws Exception {
		data.setPass(passwordEncoder.encode(data.getPass()));

		final var user = new User();
		user.setEmail(data.getEmail());
		user.setName(data.getName());
		user.setPass(data.getPass());
		user.setCreatedBy(1l);

		userRepo.save(user);

		final UserSaveResDto useSaveResDto = new UserSaveResDto();
		useSaveResDto.setId(user.getId());
		useSaveResDto.setMessage("Saved");

		return useSaveResDto;
	}

	@Override
	public UserGetByIdResDto getById(Long id) throws Exception {
		final var user = userRepo.findById(id).orElseThrow(() -> new ValidateException("User not found"));

		final UserGetByIdResDto userResult = new UserGetByIdResDto();
		userResult.setId(user.getId());
		userResult.setEmail(user.getEmail());
		userResult.setName(user.getName());

		return userResult;
	}
	
	@Override
	public Optional<User> getByEmail(String email) throws Exception {
		return userRepo.findByEmail(email);
	}

	@Override
	public List<UserListResDto> getAll() throws Exception {
		final List<UserListResDto> users = new ArrayList<>();

		userRepo.findAll().forEach(userDb -> {
			final UserListResDto user = new UserListResDto();
			user.setId(userDb.getId());
			user.setEmail(userDb.getEmail());
			user.setName(userDb.getName());

			users.add(user);
		});

		return users;  
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final User user = userRepo.findByEmail(username).orElseThrow(() -> new InvalidLoginException());

		return new org.springframework.security.core.userdetails.User(
				username, user.getPass(), new ArrayList<>()
		);
	}
}
