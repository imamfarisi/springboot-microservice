package com.lawencon.user.service;

import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.lawencon.user.dto.user.UserGetByIdResDto;
import com.lawencon.user.dto.user.UserSaveReqDto;
import com.lawencon.user.dto.user.UserListResDto;
import com.lawencon.user.dto.user.UserSaveResDto;
import com.lawencon.user.model.User;

public interface UserService extends UserDetailsService {

	UserSaveResDto save(UserSaveReqDto data) throws Exception;

	UserGetByIdResDto getById(Long id) throws Exception;

	Optional<User> getByEmail(String email) throws Exception;

	List<UserListResDto> getAll() throws Exception;
}
