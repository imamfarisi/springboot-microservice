package com.lawencon.user.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lawencon.user.dto.user.UserGetByIdResDto;
import com.lawencon.user.dto.user.UserSaveReqDto;
import com.lawencon.user.dto.user.UserListResDto;
import com.lawencon.user.dto.user.UserSaveResDto;
import com.lawencon.user.service.UserService;

@RestController
@RequestMapping("users")
public class UserController {
	private final UserService userService;
	
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<?> getAll() throws Exception {
		final List<UserListResDto> listResult = userService.getAll();
		return new ResponseEntity<List<UserListResDto>>(listResult, HttpStatus.OK);
	}

	@GetMapping("{id}") 
	public ResponseEntity<?> getById(@PathVariable("id") Long id) throws Exception {
		final UserGetByIdResDto user = userService.getById(id);
		return new ResponseEntity<UserGetByIdResDto>(user, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> save(@RequestBody UserSaveReqDto user) throws Exception {
		final UserSaveResDto userInsert = userService.save(user);
		return new ResponseEntity<UserSaveResDto>(userInsert, HttpStatus.CREATED);
	}

}
