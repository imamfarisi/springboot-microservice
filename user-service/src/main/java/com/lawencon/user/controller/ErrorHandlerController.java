package com.lawencon.user.controller;

import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.lawencon.user.constant.ResponseMessage;
import com.lawencon.user.dto.ErrorRes;
import com.lawencon.user.exception.InvalidLoginException;
import com.lawencon.user.exception.ValidateException;

@ControllerAdvice
public class ErrorHandlerController {

	@ExceptionHandler(ValidateException.class)
	public ResponseEntity<?> handleValidate(ValidateException e) {
		final var message = NestedExceptionUtils.getMostSpecificCause(e).getMessage();
		final var errorRes = new ErrorRes(message);
		return new ResponseEntity<>(errorRes, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ InvalidLoginException.class, BadCredentialsException.class })
	public ResponseEntity<?> handleUsernameNotFound() {
		final var errorRes = new ErrorRes(ResponseMessage.WRONG_USERNAME_PASWORD.getMessage());
		return new ResponseEntity<>(errorRes, HttpStatus.UNAUTHORIZED);
	}
}
