package com.lawencon.user.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lawencon.user.constant.ResponseMessage;
import com.lawencon.user.dto.ErrorRes;
import com.lawencon.user.dto.user.auth.AuthorizationResDto;

@RestController
@RequestMapping("verify")
public class VerifyController {

	@PostMapping
	public ResponseEntity<?> verifyToken() throws Exception {
		final var authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			final AuthorizationResDto data = (AuthorizationResDto) authentication.getPrincipal();
			return new ResponseEntity<>(data, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ErrorRes(ResponseMessage.INVALID_TOKEN.getMessage()), HttpStatus.UNAUTHORIZED);
	}

}
