package com.lawencon.user.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lawencon.core.util.JwtUtil;
import com.lawencon.user.dto.login.LoginReqDto;
import com.lawencon.user.dto.login.LoginResDto;
import com.lawencon.user.exception.ValidateException;
import com.lawencon.user.service.UserService;

@RestController
@RequestMapping("login")
public class LoginController {

	private static final LocalDateTime EXPIRED_DATE = LocalDateTime.now().plusDays(1);
	
	private final AuthenticationManager authenticationManager;
	private final JwtUtil jwtUtil;
	private final UserService userService;
	
	public LoginController(
			AuthenticationManager authenticationManager,
			JwtUtil jwtComponent,
			UserService userService
	) {
		this.authenticationManager = authenticationManager;
		this.jwtUtil = jwtComponent;
		this.userService = userService;
	}

	@PostMapping
	public ResponseEntity<LoginResDto> login(@RequestBody LoginReqDto data) throws Exception {
		authenticationManager.authenticate(
			new UsernamePasswordAuthenticationToken(data.getEmail(), data.getPass())
		).isAuthenticated();
		
		final var user = userService.getByEmail(data.getEmail()).orElseThrow(() -> new ValidateException("User not found"));
		
		final var claims = new HashMap<String, Object>();
		claims.put("exp", Timestamp.valueOf(EXPIRED_DATE));
		claims.put("id", user.getId());

		final var token = jwtUtil.GenerateToken(claims);

		final var loginResDto = new LoginResDto();
		loginResDto.setToken(token);

		return new ResponseEntity<LoginResDto>(loginResDto, HttpStatus.OK);
	}
}
