package com.lawencon.user.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lawencon.core.util.JwtUtil;
import com.lawencon.user.dto.ErrorRes;
import com.lawencon.user.dto.user.auth.AuthorizationResDto;

import io.jsonwebtoken.Claims;

@Component
public class AuthorizationFilter extends OncePerRequestFilter {

	private final List<RequestMatcher> antMatchers;
	private final ObjectMapper objectMapper;
	private final JwtUtil jwtUtil;

	public AuthorizationFilter(
			List<RequestMatcher> antMatchers, 
			ObjectMapper objectMapper,
			JwtUtil jwtUtil
	) {
		this.antMatchers = antMatchers;
		this.objectMapper = objectMapper;
		this.jwtUtil = jwtUtil;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		final long count = antMatchers.stream().filter(matcher -> matcher.matches(request)).collect(Collectors.counting());
		if (count == 0 && !request.getRequestURI().equals("/login")) {
			final String authorization = request.getHeader("Authorization");

			if (authorization == null || authorization.length() < 8) {
				setResponse(response, HttpStatus.UNAUTHORIZED.value(), "Invalid Token");
				return;
			}

			try {
				final String token = authorization.replaceFirst("Bearer ", "");
				final Claims claims = jwtUtil.getClaimByToken(token);

				// register data to SecurityContext
				final AuthorizationResDto authorizationResDto = new AuthorizationResDto();
				authorizationResDto.setToken(token);
				authorizationResDto.setId(claims.get("id").toString());

				final Authentication auth = new UsernamePasswordAuthenticationToken(authorizationResDto, null);
				SecurityContextHolder.getContext().setAuthentication(auth);
 
			} catch (Exception e) {
				e.printStackTrace();
				setResponse(response, HttpStatus.UNAUTHORIZED.value(), "Invalid Token");
				return;
			}

		}

		filterChain.doFilter(request, response);
	}

	private void setResponse(HttpServletResponse response, int httpStatus, String msg) throws IOException {
		final ErrorRes errorRes = new ErrorRes(msg);
		response.getWriter().append(objectMapper.writeValueAsString(errorRes));
		response.setStatus(httpStatus);
		response.setContentType("application/json");
	}

}
