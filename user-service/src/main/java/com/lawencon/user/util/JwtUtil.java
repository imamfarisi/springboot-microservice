//package com.lawencon.user.util;
//
//import java.util.Map;
//
//import javax.crypto.SecretKey;
//
//import org.springframework.stereotype.Component;
//
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;
//import io.jsonwebtoken.security.Keys;
//
//@Component
//public class JwtUtil {
//
//	private SecretKey secretKey;
//	
//	public JwtUtil() {
//		secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS256);
//	}
//	
//	public String GenerateToken(Map<String, Object> claims) {
//		final var jwtBuilder = Jwts.builder()
//				.signWith(secretKey)
//				.setClaims(claims);
//		
//		return jwtBuilder.compact();
//	}
//	
//	public Claims getClaimByToken(String token) {
//		return Jwts.parserBuilder()
//				.setSigningKey(secretKey)
//				.build()
//				.parseClaimsJws(token)
//				.getBody();
//	}
//}
