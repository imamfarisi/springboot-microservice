package com.lawencon.company.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lawencon.company.constant.ResponseMessage;
import com.lawencon.company.dto.ErrorRes;
import com.lawencon.core.util.JwtUtil;

import io.jsonwebtoken.Claims;

@Component
public class AuthorizationFilter extends OncePerRequestFilter {

	private final List<RequestMatcher> antMatchers;
	private final ObjectMapper objectMapper;
	private final JwtUtil jwtUtil;

	public AuthorizationFilter(
			List<RequestMatcher> antMatchers, 
			ObjectMapper objectMapper,
			JwtUtil jwtUtil
	) {
		this.antMatchers = antMatchers;
		this.objectMapper = objectMapper;
		this.jwtUtil = jwtUtil;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		final var count = antMatchers.stream()
				.filter(matcher -> matcher.matches(request))
				.collect(Collectors.counting());
		
		if (count == 0l) {
			final var authorization = request.getHeader("Authorization");
			if (authorization == null || authorization.length() < 8) {
				setResponse(response, 401, ResponseMessage.INVALID_TOKEN.getMessage());
				return;
			}
			
			try {
				final String token = authorization.replaceFirst("Bearer ", "");
				final Claims claims = jwtUtil.getClaimByToken(token);

				// save id to SecurityContextHolder
				final var auth = new UsernamePasswordAuthenticationToken(claims.get("id").toString(), null);
				SecurityContextHolder.getContext().setAuthentication(auth);
			} catch (Exception e) {
				e.printStackTrace();
				setResponse(response, HttpStatus.UNAUTHORIZED.value(), "Invalid Token");
				return;
			}
		}

		filterChain.doFilter(request, response);
	}

	private void setResponse(HttpServletResponse response, int httpStatus, String msg) throws IOException {
		final var errorRes = new ErrorRes(msg);
		response.getWriter().append(objectMapper.writeValueAsString(errorRes));
		response.setStatus(httpStatus);
		response.setContentType("application/json");
	}

}
