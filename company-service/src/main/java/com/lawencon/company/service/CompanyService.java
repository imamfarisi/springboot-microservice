package com.lawencon.company.service;

import java.util.List;

import com.lawencon.company.dto.CompanyInsertReq;
import com.lawencon.company.dto.CompanyInsertRes;
import com.lawencon.company.dto.CompanyListRes;

public interface CompanyService {

	CompanyInsertRes save(CompanyInsertReq data) throws Exception;

	List<CompanyListRes> getAll() throws Exception;
}
