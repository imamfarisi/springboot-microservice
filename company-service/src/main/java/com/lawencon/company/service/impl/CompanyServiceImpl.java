package com.lawencon.company.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.lawencon.company.dto.CompanyInsertReq;
import com.lawencon.company.dto.CompanyInsertRes;
import com.lawencon.company.dto.CompanyListRes;
import com.lawencon.company.model.Company;
import com.lawencon.company.repo.CompanyRepo;
import com.lawencon.company.service.CompanyService;
import com.lawencon.core.util.AuthenticationUtil;

@Service
public class CompanyServiceImpl implements CompanyService {

	private final CompanyRepo companyRepo;
	private final AuthenticationUtil authenticationUtil;

	public CompanyServiceImpl(CompanyRepo companyRepo, AuthenticationUtil authenticationUtil) {
		this.companyRepo = companyRepo;
		this.authenticationUtil = authenticationUtil;
	}

	@Override
	public CompanyInsertRes save(CompanyInsertReq data) throws Exception {
		final var company = new Company();
		company.setName(data.getName());
		company.setCreatedBy(authenticationUtil.getPrincipal().getId());
		companyRepo.save(company);

		final var userResult = new CompanyInsertRes();
		userResult.setId(company.getId());
		userResult.setMessage("Saved");

		return userResult;
	}

	@Override
	public List<CompanyListRes> getAll() throws Exception {
		final var companies = new ArrayList<CompanyListRes>();

		companyRepo.findAll().forEach(comp -> {
			final var data = new CompanyListRes();
			data.setId(comp.getId());
			data.setName(comp.getName());

			companies.add(data);
		});

		return companies;
	}

}
