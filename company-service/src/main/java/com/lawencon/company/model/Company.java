package com.lawencon.company.model;

import javax.persistence.Entity;

import com.lawencon.core.base.BaseModel;

@Entity
public class Company extends BaseModel {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
