package com.lawencon.company.constant;

public enum ResponseMessage {
	INVALID_TOKEN("Invalid Token");

	private String message;

	private ResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
