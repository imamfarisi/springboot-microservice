package com.lawencon.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lawencon.company.dto.ErrorRes;
import com.lawencon.company.exception.ValidateException;

@ControllerAdvice
public class ErrorHandlerController {
	
	@Autowired
	private ObjectMapper objectMapper;

	@ExceptionHandler(ValidateException.class)
	public ResponseEntity<?> handleValidate(ValidateException e) {
		e.printStackTrace();
		final var message = NestedExceptionUtils.getMostSpecificCause(e).getMessage();
		final var errorRes = new ErrorRes(message);
		return new ResponseEntity<>(errorRes, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(RestClientException.class)
	public ResponseEntity<?> handleRestClientException(RestClientResponseException e) {
		var errorRes = new ErrorRes("Invalid Request");
		try {
			errorRes = objectMapper.readValue(e.getResponseBodyAsString(), ErrorRes.class);
		} catch (JsonProcessingException jpe) {
			jpe.printStackTrace();
		}

		return new ResponseEntity<>(errorRes, HttpStatus.BAD_REQUEST);
	}
}
