package com.lawencon.company.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lawencon.company.dto.CompanyInsertReq;
import com.lawencon.company.dto.CompanyInsertRes;
import com.lawencon.company.dto.CompanyListRes;
import com.lawencon.company.service.CompanyService;

@RestController
@RequestMapping("companies")
public class CompanyController {

	private final CompanyService companyService;
	
	public CompanyController(CompanyService companyService) {
		this.companyService = companyService;
	}

	@GetMapping
	public ResponseEntity<?> getAll() throws Exception {
		final var companies = companyService.getAll();
		return new ResponseEntity<List<CompanyListRes>>(companies, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> save(@RequestBody CompanyInsertReq role) throws Exception {
		final var companyInsert = companyService.save(role);
		return new ResponseEntity<CompanyInsertRes>(companyInsert, HttpStatus.CREATED);
	}

}
