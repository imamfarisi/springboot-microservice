package com.lawencon.company.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lawencon.company.model.Company;

@Repository
public interface CompanyRepo extends JpaRepository<Company, Long> {

}
