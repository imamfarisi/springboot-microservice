package com.lawencon.core.util;

import javax.crypto.SecretKey;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;

public class JwtKeyGenerator {

	public static void main(String[] args) {
		final SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		final String secretString = Encoders.BASE64.encode(key.getEncoded());
		System.out.println(secretString);
	}
}
