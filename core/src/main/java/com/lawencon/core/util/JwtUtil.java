package com.lawencon.core.util;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtUtil {
	
	@Value("${secret.key}")
	private String secretKeyStr;

	public String GenerateToken(Map<String, Object> claims) {
		final var secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKeyStr));
		final var jwtBuilder = Jwts.builder()
				.signWith(secretKey)
				.setClaims(claims);
		
		return jwtBuilder.compact();
	}
	
	public Claims getClaimByToken(String token) {
		final var secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKeyStr));
		return Jwts.parserBuilder()
				.setSigningKey(secretKey)
				.build()
				.parseClaimsJws(token)
				.getBody();
	}
}
